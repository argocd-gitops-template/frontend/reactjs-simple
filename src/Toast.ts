import { toast, ToastOptions } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const OPTIONS: ToastOptions = {
  position: "bottom-center",
  autoClose: 3000,
};
const Toast = {
  info: (message: string, options?: ToastOptions) =>
    toast.info(message, {
      ...OPTIONS,
      ...options,
    }),
  success: (message: string, options?: ToastOptions) =>
    toast.success(message, {
      ...OPTIONS,
      ...options,
    }),
  warning: (message: string, options?: ToastOptions) =>
    toast.warning(message, {
      ...OPTIONS,
      ...options,
    }),
  error: (message: string, options?: ToastOptions) =>
    toast.error(message, {
      ...OPTIONS,
      ...options,
    }),
};

export default Toast;
