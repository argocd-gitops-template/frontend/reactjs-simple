### STAGE 1: Install Dependencies and Build ###
FROM node:16-alpine as builder

WORKDIR /build-stage

COPY package*.json ./

RUN npm ci

COPY . .

ARG REACT_APP_BASE_URL

ENV NODE_ENV=production
ENV REACT_APP_BASE_URL=$REACT_APP_BASE_URL

RUN npm run build

### STAGE 2: Run ###
FROM nginx

COPY --from=builder /build-stage/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /build-stage/build /usr/share/nginx/html
